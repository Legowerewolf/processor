# Processor #

Processor is an automation program. It queues and executes tasks based on configured rules. It works on Windows and is untested on other platforms.

## Getting started ##
Processor requires Java to run. Download it from [here](https://www.java.com/inc/BrowserRedirect1.jsp?locale=en) or, if you have [Chocolatey](https://chocolatey.org/) installed, run ```choco install javaruntime``` as an administrator.

Download the prebuilt package [here](https://bitbucket.org/Legowerewolf/processor/downloads/Processor.jm-1.0.jar) or see the section below about building from source.

Rules and settings are configured in a file called ```processor-config.json``` saved in your user directory or alongside the JAR. Find an annotated template [here](https://bitbucket.org/Legowerewolf/processor/src/dd6f58e61cf2a4e081cf73d4ee36e03697092d3b/processor-config.json?at=master).

## Running ##
Run the program by double-clicking the jar file. If you wish to override the settings in the config file, run ```java -jar Processor.jm-1.0.jar``` and add the appropriate arguments.

 * ```persist``` and ```nopersist``` enable and disable persistent mode.
 * ```slots:xxx``` allows ```xxx``` number of tasks to be running at once. 
 * ```delay:xxx``` makes the program wait ```xxx``` ms before evaluating the rules again. 

## Build from source ##
 Building from source requires [Maven](https://maven.apache.org/index.html) and the [Java Developer Kit (JDK)](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html). Chocolatey users can install these by running ```choco install jdk8 maven```.

1. Clone or download/extract the repository.
2. In the repository's root folder, run ```mvn package```