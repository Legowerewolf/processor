package legowerewolf.processor;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class LocalTaskHandler extends ProcessorFeature {
	int taskSlots;
	int loopDelay;
	boolean isPersistent = true;
	JSONArray ruleList;
	boolean doesOutput;

	public LocalTaskHandler(JSONObject config) {
		taskSlots = Math.toIntExact((Long) config.get("taskSlots"));
		ruleList = (JSONArray) config.get("rules");
		doesOutput = (Boolean) config.get("output");
		loopDelay = Math.toIntExact((long) config.get("loopWaitTime"));
	}

	public void run() {
		ArrayList<ProcessorThread> taskQueue = new ArrayList<ProcessorThread>();
		ArrayList<ProcessorThread> activeTasks = new ArrayList<ProcessorThread>();
		do {
			Helpers.bprintln(doesOutput, "Queueing threads...");
			for (int i = 0; i < ruleList.size() && isPersistent; i++) { //Iterate through the list of rules

				JSONObject task = (JSONObject) ruleList.get(i); //Pull a rule from the list
				String type = (String) task.get("taskType"); //Grab its type

				if (type.equals("ProcessorFileMover")) //Check and see if it's a file mover
				{
					ProcessorFileMover.queueThreads(taskQueue, (String) task.get("fileSearchType"), new File((String) task.get("fileSearchLocation")), (Boolean) task.get("isSearchRecursive"), new File((String) task.get("fileDestination")));
				} else if (type.equals("ProcessorImageConverter")) // or an image converter
				{
					ProcessorImageConverter.queueThreads(taskQueue, (String) task.get("fileSearchType"), new File((String) task.get("fileSearchLocation")), (Boolean) task.get("isSearchRecursive"), (String) task.get("fileDestinationType"));
				} else if (type.equals("ProcessorExecutor")) // or an program executor
				{
					ProcessorExecutor.queueThreads(taskQueue, (String) task.get("fileSearchType"), new File((String) task.get("fileSearchLocation")), (Boolean) task.get("isSearchRecursive"), (List<String>) task.get("programToExecute"));
				}
				Helpers.bprintln(doesOutput, "Tested rule " + (i + 1) + " of " + ruleList.size());
				if (!isPersistent) {
					System.out.println("Skipping remaining rules and execution for shutdown.");
				}
			}
			
			if (isPersistent) {
				Helpers.bprintln(doesOutput, taskQueue.size() + " threads queued. Beginning execution...");

				do {
					if (activeTasks.size() < taskSlots && taskQueue.size() >= 1) {
						ProcessorThread thread = taskQueue.remove(0);
						thread.start();
						activeTasks.add(thread);
					}
					for (int i = 0; i < activeTasks.size(); i++) {
						ProcessorThread thread = activeTasks.get(i);

						if (!thread.isAlive()) {
							activeTasks.remove(i--);
						}
					}
				} while ((activeTasks.size() > 0 || taskQueue.size() >= 1));

				Helpers.bprintln(doesOutput, "Threads executed.");
			}

			for (int i = 0; i < loopDelay && isPersistent; i++) {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		} while (isPersistent);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void terminate() {
		isPersistent = false;
		while (this.isAlive()) {
		}
		System.out.println("All local task processing concluded.");
	}

}
