package legowerewolf.processor;

import java.util.List;

import org.json.simple.JSONObject;

import sx.blah.discord.api.ClientBuilder;
import sx.blah.discord.api.IDiscordClient;
import sx.blah.discord.api.events.EventDispatcher;
import sx.blah.discord.api.events.EventSubscriber;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageEvent;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.util.DiscordException;
import sx.blah.discord.util.MissingPermissionsException;
import sx.blah.discord.util.RateLimitException;

public class DiscordBot extends ProcessorFeature {
	IDiscordClient dclient;
	List<String> censorList;
	List<Long> whitelist;
	List<Long> broadcasters;
	String aboutMessage;

	public DiscordBot(JSONObject dconfig) {
		censorList = (List<String>) dconfig.get("censored");
		whitelist = (List<Long>) dconfig.get("censorChannelWhitelist");
		broadcasters = (List<Long>) dconfig.get("allowedBroadcasters");
		aboutMessage = (String) dconfig.get("about");

		ClientBuilder clientBuilder = new ClientBuilder();
		clientBuilder.withToken((String) dconfig.get("token"));
		try {
			dclient = clientBuilder.login();
		} catch (DiscordException e) {
			System.out.println("Discord client could not log in.");
			e.printStackTrace();
		}
		System.out.println("Discord client built and connecting.");
		while (!dclient.isReady()) {
		}

		System.out.println("Registering listeners...");
		EventDispatcher dispatcher = dclient.getDispatcher(); // Gets the EventDispatcher instance for this client instance
		dispatcher.registerListener(this);

		System.out.println("Discord client ready.");
		dclient.online();
	}

	public void run() {
		for (IMessage m : dclient.getMessages()) {
			applyFilter(m);
		}
	}

	@EventSubscriber
	public void onMessageReceivedEvent(MessageEvent event) {
		applyFilter(event.getMessage());
		if (!event.getMessage().isDeleted())
			handleCommand(event.getMessage());
	}

	public void applyFilter(IMessage m) {
		if (!m.isDeleted() && !whitelist.contains(m.getChannel().getLongID())) {
			boolean matchesCensorWord = false;
			String content = m.getContent().replaceAll("[,.!]", "").toLowerCase();
			for (String ls : censorList) {
				if (content.contains(" " + ls + " ") || content.startsWith(ls + " ") || content.endsWith(" " + ls) || content.equals(ls)) {
					matchesCensorWord = true;
				}
			}
			if (matchesCensorWord) {
				try {
					m.delete();
				} catch (MissingPermissionsException e) {
					System.out.println("This client doesn't have the required permissions to delete messages on this server.");
					e.printStackTrace();
				} catch (RateLimitException e) {
					System.out.println("This client has tried to delete too many messages too quickly.");
					e.printStackTrace();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

	}

	public void handleCommand(IMessage m) {
		if (m.getContent().startsWith(">")) {
			int commandStringEnd = (m.getContent().indexOf(" ") > 0) ? m.getContent().indexOf(" ") : m.getContent().length();
			switch (m.getContent().toLowerCase().substring(1, commandStringEnd)) {
			case "hello-world":
				try {
					m.getChannel().sendMessage("Hello!");
				} catch (MissingPermissionsException | RateLimitException | DiscordException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				break;
			case "xsm":
				long destChannelID = Long.parseLong(m.getContent().split(" ")[1]);
				try {
					dclient.getChannelByID(destChannelID).sendMessage("**" + m.getAuthor().getName() + "** says: " + m.getContent().substring(m.getContent().indexOf(" ", m.getContent().indexOf(" ") + 1)));
				} catch (MissingPermissionsException | RateLimitException | DiscordException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				break;
			case "help":
				try {
					m.getChannel().sendMessage("Use these wisely.\n>about - provides info about the bot\n>xsm [numerical channel ID] [message] - send a message across servers");
				} catch (MissingPermissionsException | RateLimitException | DiscordException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				break;
			case "about":
				try {
					m.getChannel().sendMessage(aboutMessage);
				} catch (MissingPermissionsException | RateLimitException | DiscordException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				break;
			case "broadcast":
				if (broadcasters.contains(m.getAuthor().getLongID())) {
					try {
						for (IChannel c : dclient.getChannels()) {
							c.sendMessage("** Broadcast from " + m.getAuthor().getName() + ":** " + m.getContent().substring(m.getContent().indexOf(" ")));
						}
					} catch (MissingPermissionsException | RateLimitException | DiscordException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				break;
			}
		}
	}

	public void terminate() {
		try {
			dclient.idle();
			dclient.logout();
		} catch (DiscordException e) {
			System.out.println("Discord client could not log out.");
			e.printStackTrace();
		}
	}

}
