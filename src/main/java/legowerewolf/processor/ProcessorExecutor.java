package legowerewolf.processor;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class ProcessorExecutor extends ProcessorThread {
	private List<String> command;

	public ProcessorExecutor(List<String> _command) {
		command = _command;
	}

	public static void queueThreads(ArrayList<ProcessorThread> arr, String fileType, File searchLocation, boolean isSearchRecursive, List<String> list) {
		ArrayList<File> fileIndex = Helpers.listFiles(searchLocation, isSearchRecursive);

		for (File f : fileIndex) {
			if (fileType.equals(Helpers.getFileType(f)) || fileType.equals("*")) {
				arr.add(new ProcessorExecutor(list));
			}
		}
	}

	public void run() {
		boolean processExists = false;
		try {
			String line;
			Process p = Runtime.getRuntime().exec("tasklist");
			BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
			while ((line = input.readLine()) != null) {
				processExists = line.contains(new File(command.get(0)).getName()) ? true : processExists;
			}
			input.close();
		} catch (Exception err) {
			err.printStackTrace();
		}
		if (!processExists) {
			try {
				@SuppressWarnings("unused")
				Process p = new ProcessBuilder(command).start();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		System.out.println("Executed " + new File(command.get(0)).getName() + ".");
	}
}
