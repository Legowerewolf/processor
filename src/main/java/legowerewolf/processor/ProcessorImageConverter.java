package legowerewolf.processor;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;

public class ProcessorImageConverter extends ProcessorThread {

	private File imageFile;
	private String destFileType;
	private static ArrayList<String> allowedTypes = Helpers.toArrayList(new String[] { "bmp", "jpg", "jpeg", "png", "pnm", "psd", "tiff" });

	public ProcessorImageConverter(File candidate, String _destFileType) {
		imageFile = candidate;
		destFileType = _destFileType;
	}

	public static void queueThreads(ArrayList<ProcessorThread> arr, String sourceFileType, File searchLocation, boolean isSearchRecursive, String destFileType) {
		ArrayList<File> fileIndex = Helpers.listFiles(searchLocation, isSearchRecursive);

		for (File candidate : fileIndex) {
			if (Helpers.getFileType(candidate).equals(sourceFileType)) { //Handles converting from a specific filetype to another filetype
				arr.add(new ProcessorImageConverter(candidate, destFileType));
			} else if (sourceFileType.equals("*") && allowedTypes.contains(Helpers.getFileType(candidate)) && !destFileType.equals(Helpers.getFileType(candidate))) { //Handles converting from any supported filetype to another filetype
				arr.add(new ProcessorImageConverter(candidate, destFileType));
			}
		}
	}

	public void run() {
		if (imageFile.exists() && imageFile.canWrite()) {
			File newLocationPlaceholder = new File(imageFile.getPath().concat("." + destFileType));
			BufferedImage image;
			try {
				image = ImageIO.read(imageFile);
				BufferedImage newImage = new BufferedImage(image.getWidth(), image.getHeight(), BufferedImage.TYPE_INT_RGB);
				newImage.createGraphics().drawImage(image, 0, 0, Color.WHITE, null);
				ImageIO.write(newImage, destFileType, newLocationPlaceholder);

				System.out.println("Converted " + imageFile.getName() + " to " + newLocationPlaceholder.getName());
				imageFile.delete();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
