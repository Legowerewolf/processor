package legowerewolf.processor;

import java.io.File;
import java.util.ArrayList;

public class ProcessorFileMover extends ProcessorThread {

	private File destination;
	private File fileToMove;

	public ProcessorFileMover(File _fileToMove, File _destination) {
		fileToMove = _fileToMove;
		destination = _destination;
	}

	public static void queueThreads(ArrayList<ProcessorThread> arr, String fileType, File searchLocation, boolean isSearchRecursive, File destination) {

		ArrayList<File> fileIndex = Helpers.listFiles(searchLocation, isSearchRecursive);

		for (File candidate : fileIndex) {
			if (Helpers.getFileType(candidate).equals(fileType.toLowerCase()) || fileType.equals("*")) {
				arr.add(new ProcessorFileMover(candidate, destination));
			}
		}
	}

	public void run() {
		if (fileToMove.exists() && fileToMove.canWrite()) {
			File newLocationPlaceholder = new File(destination.getPath() + "\\" + fileToMove.getName());
			fileToMove.renameTo(newLocationPlaceholder);
			System.out.println("Moved " + fileToMove.getName() + " to " + destination.getPath());
		}
	}
}
