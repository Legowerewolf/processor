package legowerewolf.processor;

import java.awt.AWTException;
import java.awt.SystemTray;
import java.awt.TrayIcon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class ProcessorApp {

	static boolean isPersistent = false;
	static JSONObject config = null;
	static ArrayList<ProcessorFeature> loadedFeatures = null;
	static TrayIcon icon = null;
	static boolean terminating = false;

	public static void main(String[] args) {

		System.out.println("Starting...");

		//Build tray icon
		icon = new TrayIcon(Helpers.getImage("res/icon.png"), "Processor");
		icon.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				exit();
				System.out.println("Shutting down...");
			}
		});
		icon.setImageAutoSize(true);
		try {
			SystemTray.getSystemTray().add(icon);
		} catch (AWTException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}

		//Set the shutdown hook
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				exit();
			}
		});

		// Load settings from configuration file
		try {
			//Read the file
			File configFile = null;
			if (new File(System.getProperty("user.dir") + "\\processor-config.json").exists()) {
				configFile = new File(System.getProperty("user.dir") + "\\processor-config.json");
			} else if (new File(System.getProperty("user.home") + "\\processor-config.json").exists()) {
				configFile = new File(System.getProperty("user.home") + "\\processor-config.json");
			}

			BufferedReader fileReader = new BufferedReader(new FileReader(configFile));
			String line = null;
			String fileContents = "";
			while ((line = fileReader.readLine()) != null) {
				fileContents = fileContents.concat(line);
			}
			fileReader.close();

			//Parse the file 
			JSONParser parser = new JSONParser();
			config = (JSONObject) parser.parse(fileContents);

			//Get the settings object and read the values
			JSONObject settings = (JSONObject) config.get("settings");
			isPersistent = (Boolean) settings.get("isPersistent");
		} catch (FileNotFoundException e1) {
			System.out.println("Config file could not be found.");
			e1.printStackTrace();
		} catch (IOException e) {
			System.out.println("Config file could not be loaded.");
			e.printStackTrace();
		} catch (ParseException e) {
			System.out.println("Config file could not be parsed.");
			e.printStackTrace();
		}

		// Load settings from command-line arguments
		for (String s : args) {
			if (s.equals("persist")) {
				isPersistent = true;
			} else if (s.equals("nopersist")) {
				isPersistent = false;
			}
		}

		//Load extra features
		loadedFeatures = new ArrayList<ProcessorFeature>();

		JSONArray features = (JSONArray) config.get("features");

		for (int i = 0; i < features.size(); i++) {
			JSONObject o = (JSONObject) features.get(i);
			ProcessorFeature f = null;
			if (o.get("featureType").toString().equals("discord")) {
				f = new DiscordBot((JSONObject) features.get(i));
			} else if (o.get("featureType").toString().equals("localTasks")) {
				f = new LocalTaskHandler((JSONObject) features.get(i));
			}

			f.start();
			loadedFeatures.add(f);
		}
		System.out.println(loadedFeatures.size()+ " features were loaded.");

		while (isPersistent) {
		}

		exit();
	}

	public static void exit() {
		if (terminating)
			return;
		terminating = true;
		for (ProcessorFeature f : loadedFeatures) {
			f.terminate();
		}

		System.out.println("Terminated.");
		System.exit(0);
	}

}
