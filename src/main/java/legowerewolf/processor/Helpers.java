package legowerewolf.processor;

import java.awt.Image;
import java.awt.Toolkit;
import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;

public class Helpers {
	public static void bprintln(boolean willOutput, String stringToPrint) {
		if (willOutput) {
			System.out.println(stringToPrint);
		}
	}

	@SafeVarargs
	public static <T> T[] concat(T[]... arrays) {
		ArrayList<T> al = new ArrayList<T>();
		for (T[] one : arrays)
			Collections.addAll(al, one);
		return (T[]) al.toArray(arrays[0].clone());
	}

	public static String getFileType(File f) {
		return f.toString().toLowerCase().substring(f.toString().lastIndexOf(".") + 1);
	}

	public static Image getImage(final String pathAndFileName) {
		final URL url = Thread.currentThread().getContextClassLoader().getResource(pathAndFileName);
		return Toolkit.getDefaultToolkit().getImage(url);
	}

	public static <T> boolean listContains(T[] l, T s)
	{
		boolean ret = false;
		for (T ls : l)
		{
			if (ls.equals(s))
				ret = true;
			
		}
		return ret;
	}

	public static ArrayList<File> listFiles(File directory, boolean isRecursive) {
		ArrayList<File> farr = new ArrayList<File>();
		File[] flist = directory.listFiles();

		for (File f : flist) {
			if (isRecursive && f.isDirectory()) {
				farr.addAll(listFiles(f, true));
			} else if (f.isFile()) {
				farr.add(f);
			}
		}
		return farr;
	}
	
	public static <T> ArrayList<T> toArrayList(T[] arr) {
		ArrayList<T> ret = new ArrayList<T>();
		for (T s : arr)
			ret.add(s);
		return ret;
	}
}
